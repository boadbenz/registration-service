
My name is Pattaraphon Bantha , You Can Call Me BENZ!!!

# registration-service

# Requirement

1. Service can be provide API for register to Customer (RESTful Service), Encrypt Password(BCryptPasswordEncoder) spring-security

2. Service can be provice API login for generate Token (JWT) to clients *** Token Expired in 10 days

3. After Login Clients can be Using Token(JWT) For Call Api getUserInfomation From Service

4. Service can be provide API documents in swagger-ui

# You Will know

1. Develop in Springboot Framework

2. Using JWT, AOP , Spring-security, JPA, RESTFul, Repository, JUNIT, Mockito, Maven Build tools, BCryptPasswordEncoder, Authorization, Authentication, Swagger

# ER-Diagram

<img src="er-diagram.png" />

# Database

In Project, I'm Config Default H2 DB , When Start Application It will Create Table Automatic , Database will keep data in memory

<img src="h2config.png" />

if you want to connect other sql database you can config what you want.

# Installation

**required Git, Maven, Eclipse or Intellij, Postman

>>>

git clone https://gitlab.com/boadbenz/registration-service.git

 cd registration-service

 mvn install

 mvn spring-boot:run

>>>

# How to use

<img src="swagger.png" />

Entry Below Url you will see document api
>>> 
http://localhost:8899/swagger-ui.html
>>>

# Register

1.1 Successful Case

>>> 
http://localhost:8899/api/v1/registration/register    // HTTP.POST

>>> 

```javascript
//header
{
  "Content-Type" : "application/json"
}
//body
{
  "address": "The series Condo",
  "password": "boadbenz001",
  "phone": "0836486229",
  "salary": 34400,    // you can change    Platinum (salary > 50,000 baht) 
                                        // Gold (salary between 30,000 to 50,000)
                                        // Silver (salary < 30,000)
  "username": "boadbenz"
}
```

response

```javascript

{
    "response_code": "Regis-200",
    "response_message": "Successful",
    "technical_message": "Successful",
    "namespace": null,
    "member_type": "Gold"
}

```

<img src="register.png" />


1.2 Register Fails Salary < 15000


<img src="pic-fail-case.png">

Response

```javascript

{
    "response_code": "Regis-001",
    "response_message": "Cannot Register Salary Less Than 15,000",
    "technical_message": "salary < 15,000",
    "namespace": null
}

```

# Login

>>>
http://localhost:8899/login    // HTTP.POST
>>>

<img src="login-success.png" />

Request

```javascript
//body
{
  "username": "boadbenz",
  "password": "boadbenz001"
}
```

Response --> Will Receive Header Token (JWT)

```javascript
// Header Response

{ 
  ...
  "authorization" : "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJib2FkYmVueiIsImV4cCI6MTUzNjEzOTQ2Mn0.QUHPRuPGYfRfCanY0MKPaW9zAMu1El5a02jny-aa9sIDedSV7jo6zsfn9AkJx9IpbQiD1H41u2iPz5zY6dy0yQ" // Token Different Always When Generate
  ...
}

```

# Get Infomation 

<img src="getinfo.png">

3.1 Successful Case (Gold)

>>>
http://localhost:8899/api/v1/registration/get-info?username=boadbenz // HTTP.GET  // you can change paramameter username 
>>>

Request

```javascript
//header
{
  "Content-Type" : "application/json"
  "Authorization" : "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJib2FkYmVueiIsImV4cCI6MTUzNjEzOTQ2Mn0.QUHPRuPGYfRfCanY0MKPaW9zAMu1El5a02jny-aa9sIDedSV7jo6zsfn9AkJx9IpbQiD1H41u2iPz5zY6dy0yQ" // Copy Your Token "authorization" : "Bearer ****** " From Login
}

```

Response

```javascript

{
    "response_code": "Regis-200",
    "response_message": "Successful",
    "technical_message": "Successful",
    "namespace": null,
    "username": "boadbenz",
    "password": "$2a$10$3x8SNziLjEzWRdNN9TMGluNiVn.9Zdq2gslHWBYF/CVzop5GXIfuy",
    "address": "The series Condo",
    "phone": "0836486229",
    "salary": 34400,
    "reference_code": 201808266229,
    "member_type": "Gold"
}
```

3.2 Success case (Silver)

Response

```javascript
//response
{
    "response_code": "Regis-200",
    "response_message": "Successful",
    "technical_message": "Successful",
    "namespace": null,
    "username": "patt123213",
    "password": "$2a$10$PeY4zcSNkPFTAFCeJSbKOeKPHzlDS6/cAUKZNrx/BhaTq9oPrxe.y",
    "address": "The series Condo",
    "phone": "0836486229",
    "salary": 20000,
    "reference_code": 201808266229,
    "member_type": "Silver"
}
```

3.3 Scuccess case (Platinum)

Response

```javascript
//response
{
    "response_code": "Regis-200",
    "response_message": "Successful",
    "technical_message": "Successful",
    "namespace": null,
    "username": "boadbenz11zz",
    "password": "$2a$10$sHCbaZeDz03lgZc2a520uu.9370jE8dXghqfa4eRVg4/RV8XLz6f6",
    "address": "The series Condo",
    "phone": "0836486229",
    "salary": 66000,
    "reference_code": 201808266229,
    "member_type": "Platinum"
}
```

# Coverage JUNIT (Unit test)

controller

<img src="coverage-controller.png" />


service

<img src="coverate-service.png" />

