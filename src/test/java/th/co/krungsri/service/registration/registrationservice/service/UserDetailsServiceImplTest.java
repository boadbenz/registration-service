package th.co.krungsri.service.registration.registrationservice.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import th.co.krungsri.service.registration.registrationservice.entity.Member;
import th.co.krungsri.service.registration.registrationservice.entity.MemberType;
import th.co.krungsri.service.registration.registrationservice.repository.MemberRepository;

import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsServiceImplTest {

    private static final String USERNAME = "boadbenz";

    private static final String ADDRESS = "2/17 condo the series, udomsuk 29";

    private static final String PASSWORD_ENCRYPT = "$2a$10$FRPaSW0kz3yZFDHaQA5yiet/m/M9t1Y5OToL5vWagi2CYQivicJIu";

    private static final Long REFERENCE_CODE = 201808266229l;

    private static final String PHONE = "083-648-6229";

    private static final Long SALARY = 65000l;

    @Mock
    private MemberRepository memberRepository;

    private UserDetailsServiceImpl userDetailsService;

    @Before
    public void setUp() throws Exception {
        userDetailsService = new UserDetailsServiceImpl(memberRepository);

    }

    @Test
    public void loadUserByUsernameSuccessful() {

        MemberType memberType = new MemberType();
        memberType.setName("Platinum");

        Member member = new Member();
        member.setUsername(USERNAME);
        member.setPassword(PASSWORD_ENCRYPT);
        member.setAddress(ADDRESS);
        member.setReferenceCode(REFERENCE_CODE);
        member.setPhone(PHONE);
        member.setSalary(SALARY);
        member.setMemberType(memberType);

        User user = new User(member.getUsername(), member.getPassword(), emptyList());


        when(memberRepository.findByUsername(USERNAME)).thenReturn(member);

        UserDetails userAssert = userDetailsService.loadUserByUsername(USERNAME);

        assertEquals(user.getUsername(), userAssert.getUsername());
        assertEquals(user.getPassword(), userAssert.getPassword());

    }

    @Test (expected=UsernameNotFoundException.class)
    public void loadUserByUsernameNotFound(){

        when(memberRepository.findByUsername(USERNAME)).thenReturn(null);

        userDetailsService.loadUserByUsername(USERNAME);

    }
}