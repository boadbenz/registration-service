package th.co.krungsri.service.registration.registrationservice.controller;

import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import th.co.krungsri.service.registration.registrationservice.model.BaseModelResponse;
import th.co.krungsri.service.registration.registrationservice.model.RegisterModel;
import th.co.krungsri.service.registration.registrationservice.model.RegisterResponse;
import th.co.krungsri.service.registration.registrationservice.model.UserInfoResponse;
import th.co.krungsri.service.registration.registrationservice.service.RegistrationService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static th.co.krungsri.service.registration.registrationservice.security.SecurityConstants.SIGN_UP_URL;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(secure = false)
public class RegistrationControllerTest {

    private static final String USERNAME = "boadbenz";

    private static final String ADDRESS = "2/17 condo the series, udomsuk 29";

    private static final String PASSWORD = "boadbenz001";

    private static final String PASSWORD_ENCRYPT = "$2a$10$FRPaSW0kz3yZFDHaQA5yiet/m/M9t1Y5OToL5vWagi2CYQivicJIu";

    private static final Long REFERENCE_CODE = 201808266229l;

    private static final String PHONE = "083-648-6229";

    private static final Long SALARY = 65000l;

    private static final String MEMBER_TYPE = "Platinum";

    private static final String RESPONSE_CODE = "Regis-200";

    private static final String RESPONSE_MESSAGE = "Successful";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    RegistrationService registrationService;

    @Test
    public void registerSuccessful() throws Exception {
        // Mock Request Body

        RegisterModel registerModel = new RegisterModel();
        registerModel.setUsername(USERNAME);
        registerModel.setPassword(PASSWORD);
        registerModel.setAddress(ADDRESS);
        registerModel.setPhone(PHONE);
        registerModel.setSalary(SALARY);
        Gson gson = new Gson();
        String json = gson.toJson(registerModel);

        //Mock Response

        RegisterResponse registerResponse = new RegisterResponse();

        registerResponse.setResponseCode(RESPONSE_CODE);
        registerResponse.setResponseMessage(RESPONSE_MESSAGE);
        registerResponse.setTechnicalMessage(RESPONSE_MESSAGE);
        registerResponse.setMemberType(MEMBER_TYPE);

        when(registrationService.register(registerModel)).thenReturn(registerResponse);

        this.mockMvc.perform(post(SIGN_UP_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.response_code" , is(RESPONSE_CODE)));

    }

    @Test
    public void getUserInfo() throws Exception {
        UserInfoResponse userInfoResponse = new UserInfoResponse();
        userInfoResponse.setResponseCode(RESPONSE_CODE);
        userInfoResponse.setResponseMessage(RESPONSE_MESSAGE);
        userInfoResponse.setTechnicalMessage(RESPONSE_MESSAGE);
        userInfoResponse.setPassword(PASSWORD_ENCRYPT);
        userInfoResponse.setUsername(USERNAME);
        userInfoResponse.setAddress(ADDRESS);
        userInfoResponse.setPhone(PHONE);
        userInfoResponse.setSalary(SALARY);
        userInfoResponse.setReferenceCode(REFERENCE_CODE);
        userInfoResponse.setMemberType(MEMBER_TYPE);

        when(registrationService.getUserInfo(USERNAME)).thenReturn(userInfoResponse);

        this.mockMvc.perform(get("/api/v1/registration/get-info")
                .param("username" , USERNAME)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.response_code" , is(RESPONSE_CODE)));

    }
}