package th.co.krungsri.service.registration.registrationservice.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import th.co.krungsri.service.registration.registrationservice.entity.Member;
import th.co.krungsri.service.registration.registrationservice.entity.MemberType;
import th.co.krungsri.service.registration.registrationservice.exception.apiexception.ApplicationException;
import th.co.krungsri.service.registration.registrationservice.model.RegisterModel;
import th.co.krungsri.service.registration.registrationservice.model.RegisterResponse;
import th.co.krungsri.service.registration.registrationservice.model.UserInfoResponse;
import th.co.krungsri.service.registration.registrationservice.repository.MemberRepository;
import th.co.krungsri.service.registration.registrationservice.repository.MemberTypeRepository;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class RegistrationServiceImplTest {

    private static final String USERNAME = "boadbenz";

    private static final String ADDRESS = "2/17 condo the series, udomsuk 29";

    private static final String PASSWORD = "boadbenz001";

    private static final String PASSWORD_ENCRYPT = "$2a$10$FRPaSW0kz3yZFDHaQA5yiet/m/M9t1Y5OToL5vWagi2CYQivicJIu";

    private static final Long REFERENCE_CODE = 201808266229l;

    private static final String PHONE = "083-648-6229";

    private static final Long SALARY = 65000l;

    private static final String PLATINUM = "Platinum";

    private static final String GOLD = "Gold";

    private static final String SILVER = "Silver";

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Mock
    private MemberTypeRepository memberTypeRepository;

    @Mock
    private MemberRepository memberRepository;

    private RegistrationServiceImpl registrationService;

    @Before
    public void setUp() throws Exception {
        registrationService = new RegistrationServiceImpl(bCryptPasswordEncoder,memberTypeRepository, memberRepository);

    }

    @Test
    public void registerPlatinumSuccessful() throws ApplicationException {

        RegisterResponse registerResponse = new RegisterResponse();
        registerResponse.setMemberType(PLATINUM);

        RegisterModel registerModel = new RegisterModel();
        registerModel.setUsername(USERNAME);
        registerModel.setPassword(PASSWORD);
        registerModel.setAddress(ADDRESS);
        registerModel.setPhone(PHONE);
        registerModel.setSalary(50000l);

        Member entityMember = new Member();

        MemberType memberTypePlatinum = new MemberType();
        memberTypePlatinum.setName(PLATINUM);

        MemberType memberTypeGold = new MemberType();
        memberTypeGold.setName(GOLD);

        MemberType memberTypeSilver = new MemberType();
        memberTypeSilver.setName(SILVER);

        entityMember.setPhone(PHONE);
        entityMember.setUsername(registerModel.getUsername());
        entityMember.setSalary(registerModel.getSalary());
        entityMember.setAddress(registerModel.getAddress());
        entityMember.setPassword(PASSWORD_ENCRYPT);
        entityMember.setReferenceCode(REFERENCE_CODE);

        when(memberTypeRepository.findByName(RegistrationServiceImpl.Type.Platinum.toString())).thenReturn(memberTypePlatinum);

        when(bCryptPasswordEncoder.encode(registerModel.getPassword())).thenReturn(PASSWORD_ENCRYPT);

        RegisterResponse registerResponseAssert = registrationService.register(registerModel);

        assertEquals(registerResponse, registerResponseAssert);

    }

    @Test
    public void registerGoldSuccessful() throws ApplicationException {

        RegisterResponse registerResponse = new RegisterResponse();
        registerResponse.setMemberType(GOLD);

        RegisterModel registerModel = new RegisterModel();
        registerModel.setUsername(USERNAME);
        registerModel.setPassword(PASSWORD);
        registerModel.setAddress(ADDRESS);
        registerModel.setPhone(PHONE);
        registerModel.setSalary(40000l);

        Member entityMember = new Member();

        MemberType memberTypeGold = new MemberType();
        memberTypeGold.setName(GOLD);

        entityMember.setPhone(PHONE);
        entityMember.setUsername(registerModel.getUsername());
        entityMember.setSalary(registerModel.getSalary());
        entityMember.setAddress(registerModel.getAddress());
        entityMember.setPassword(PASSWORD_ENCRYPT);
        entityMember.setReferenceCode(REFERENCE_CODE);


        when(memberTypeRepository.findByName(RegistrationServiceImpl.Type.Gold.toString())).thenReturn(memberTypeGold);

        when(bCryptPasswordEncoder.encode(registerModel.getPassword())).thenReturn(PASSWORD_ENCRYPT);

        RegisterResponse registerResponseAssert = registrationService.register(registerModel);

        assertEquals(registerResponse, registerResponseAssert);

    }

    @Test
    public void registerSilverSuccessful() throws ApplicationException {

        RegisterResponse registerResponse = new RegisterResponse();
        registerResponse.setMemberType(SILVER);

        RegisterModel registerModel = new RegisterModel();
        registerModel.setUsername(USERNAME);
        registerModel.setPassword(PASSWORD);
        registerModel.setAddress(ADDRESS);
        registerModel.setPhone(PHONE);
        registerModel.setSalary(20000l);

        Member entityMember = new Member();

        MemberType memberTypeSilver = new MemberType();
        memberTypeSilver.setName(SILVER);

        entityMember.setPhone(PHONE);
        entityMember.setUsername(registerModel.getUsername());
        entityMember.setSalary(registerModel.getSalary());
        entityMember.setAddress(registerModel.getAddress());
        entityMember.setPassword(PASSWORD_ENCRYPT);
        entityMember.setReferenceCode(REFERENCE_CODE);


        when(memberTypeRepository.findByName(RegistrationServiceImpl.Type.Silver.toString())).thenReturn(memberTypeSilver);

        when(bCryptPasswordEncoder.encode(registerModel.getPassword())).thenReturn(PASSWORD_ENCRYPT);

        RegisterResponse registerResponseAssert = registrationService.register(registerModel);

        assertEquals(registerResponse, registerResponseAssert);

    }

    @Test(expected=ApplicationException.class)
    public void registerSalaryLessThan15000() throws ApplicationException {

        RegisterResponse registerResponse = new RegisterResponse();
        registerResponse.setMemberType(SILVER);

        RegisterModel registerModel = new RegisterModel();
        registerModel.setUsername(USERNAME);
        registerModel.setPassword(PASSWORD);
        registerModel.setAddress(ADDRESS);
        registerModel.setPhone(PHONE);
        registerModel.setSalary(500l);

        when(bCryptPasswordEncoder.encode(registerModel.getPassword())).thenReturn(PASSWORD_ENCRYPT);

        registrationService.register(registerModel);

    }

    @Test
    public void getUserInfoSuccessful() {
        MemberType memberType = new MemberType();
        memberType.setName("Platinum");

        Member member = new Member();
        member.setUsername(USERNAME);
        member.setPassword(PASSWORD_ENCRYPT);
        member.setAddress(ADDRESS);
        member.setReferenceCode(REFERENCE_CODE);
        member.setPhone(PHONE);
        member.setSalary(SALARY);
        member.setMemberType(memberType);

        UserInfoResponse userInfoResponse = new UserInfoResponse();
        userInfoResponse.setAddress(member.getAddress());
        userInfoResponse.setPhone(member.getPhone());
        userInfoResponse.setSalary(member.getSalary());
        userInfoResponse.setUsername(member.getUsername());
        userInfoResponse.setReferenceCode(member.getReferenceCode());
        userInfoResponse.setPassword(member.getPassword());
        userInfoResponse.setMemberType(member.getMemberType().getName());

        when(memberRepository.findByUsername(USERNAME)).thenReturn(member);

        UserInfoResponse userInfoResponseAssert = registrationService.getUserInfo(USERNAME);

        assertEquals(userInfoResponse, userInfoResponseAssert);

    }
}