package th.co.krungsri.service.registration.registrationservice.config;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import th.co.krungsri.service.registration.registrationservice.utils.Utility;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Aspect
@Slf4j
@Component
public class AOPService {

    private static final String PROPERTIES_RESPONSE_CODE = "responseCode";
    private static final String PROPERTIES_RESPONSE_MESSAGE = "responseMessage";
    private static final String PROPERTIES_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String DEFAULT_RESPONSE_RESPONSE_NUMBER = "Regis-200";
    private static final String DEFAULT_RESPONSE_MESSAGE_SUCCESS = "Successful";

    @Around("execution(* th.co.krungsri.service.registration.registrationservice.controller..*(..))")
    public Object executeController(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        return loggingAOP(proceedingJoinPoint);
    }

    @AfterReturning(pointcut = "execution(* th.co.krungsri.service.registration.registrationservice.controller..*(..))", returning = "result")
    public void executeProcress(JoinPoint joinPoint, Object result) throws Throwable {
        handleProcess(joinPoint, result);
    }

    public Object loggingAOP(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        StopWatch watch = new StopWatch();
        Object[] arguments = proceedingJoinPoint.getArgs();
        String request = null;
        if (arguments.length != 0) {
            request = Arrays.asList(arguments).toString();
        }
        log.info("================  signature : {} ================ ", proceedingJoinPoint.getSignature());
        log.info("================  request : {} ================ ", request);

        Object returnObj = null;
        try {
            watch.start();
            returnObj = proceedingJoinPoint.proceed();
            watch.stop();
        } catch (Throwable throwable) {
            watch.stop();
            log.error(" ================  {}() Exception :  ================ ", proceedingJoinPoint.getSignature().getName(), throwable);
            throw throwable;
        }
        return returnObj;
    }

    public void handleProcess(JoinPoint joinPoint, Object returnObj) throws Throwable {
        try {
            Method[] methods = returnObj.getClass().getDeclaredMethods();
            List<String> mehthodNameLst = new ArrayList<String>();
            for (Method method : methods) {
                mehthodNameLst.add(method.getName());
            }

            setValueMethod(returnObj, PROPERTIES_RESPONSE_CODE,DEFAULT_RESPONSE_RESPONSE_NUMBER);

            setValueMethod(returnObj, PROPERTIES_RESPONSE_MESSAGE, DEFAULT_RESPONSE_MESSAGE_SUCCESS);

            setValueMethod(returnObj, PROPERTIES_TECHNICAL_MESSAGE, DEFAULT_RESPONSE_MESSAGE_SUCCESS);

            log.info("================  response : {} ================ ", returnObj);
        } catch (Throwable throwable) {
            throw throwable;
        }
    }

    private void setValueMethod(Object returnObj, String methodName, String value) throws Throwable {
        Method method = returnObj.getClass().getMethod(Utility.doSetMethod(methodName),
                new Class[] { String.class });
        method.invoke(returnObj, new Object[] { value });
    }
}
