package th.co.krungsri.service.registration.registrationservice.service;

import th.co.krungsri.service.registration.registrationservice.exception.apiexception.ApplicationException;
import th.co.krungsri.service.registration.registrationservice.model.RegisterModel;
import th.co.krungsri.service.registration.registrationservice.model.RegisterResponse;
import th.co.krungsri.service.registration.registrationservice.model.UserInfoResponse;

public interface RegistrationService {
    RegisterResponse register(RegisterModel member) throws ApplicationException;

    UserInfoResponse getUserInfo(String userName);
}
