package th.co.krungsri.service.registration.registrationservice.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "MEMBER_TYPE")
public class MemberType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "member_type_Sequence")
    @SequenceGenerator(name = "member_type_Sequence", sequenceName = "MEMBER_TYPE_SEQ")
    private Long id;

    @Column(name = "name")
    @JsonProperty(value = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "memberType")
    @JsonProperty(value = "members")
    private List<Member> members;
}
