package th.co.krungsri.service.registration.registrationservice.exception;

import lombok.Getter;
import lombok.Setter;
import th.co.krungsri.service.registration.registrationservice.model.BaseModelResponse;

@Getter
@Setter
public abstract class ApplicationRuntimeException extends Exception{

    private BaseModelResponse baseModelResponse;

    public ApplicationRuntimeException(String code,
                                       String responseMessage,
                                       String technicalMessage) {
        super(code);
        this.baseModelResponse = new BaseModelResponse();
        this.baseModelResponse.setResponseCode(code);
        this.baseModelResponse.setResponseMessage(responseMessage);
        this.baseModelResponse.setTechnicalMessage(technicalMessage);
    }

}
