package th.co.krungsri.service.registration.registrationservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "MEMBER")
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "member_Sequence")
    @SequenceGenerator(name = "member_Sequence", sequenceName = "MEMBER_SEQ")
    private Long id;

    @JsonProperty(value = "username")
    @Column(name = "username")
    private String username;

    @JsonProperty(value = "password")
    @Column(name = "password")
    private String password;

    @JsonProperty(value = "address")
    @Column(name = "address")
    private String address;

    @JsonProperty(value = "phone")
    @Column(name = "phone")
    private String phone;

    @JsonProperty(value = "salary")
    @Column(name = "salary")
    private Long salary;

    @JsonProperty(value = "reference_code")
    @Column(name = "reference_code")
    private Long referenceCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty(value = "member_type")
    @JoinColumn(name = "member_type_id", nullable = false)
    private MemberType memberType;

    public Member(){

    }
}
