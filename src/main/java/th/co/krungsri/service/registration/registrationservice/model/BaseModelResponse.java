package th.co.krungsri.service.registration.registrationservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BaseModelResponse {

    @JsonProperty("response_code")
    protected java.lang.String responseCode;
    @JsonProperty("response_message")
    protected java.lang.String responseMessage;
    @JsonProperty("technical_message")
    protected java.lang.String technicalMessage;
    @JsonProperty("namespace")
    protected java.lang.String namespace;
}
