package th.co.krungsri.service.registration.registrationservice.repository;

import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import th.co.krungsri.service.registration.registrationservice.entity.Member;

@RepositoryRestResource(collectionResourceRel = "member",path = "member")
public interface MemberRepository extends BaseRepository<Member, Long> {

    Member findByUsername(@Param("username") String username);
}
