package th.co.krungsri.service.registration.registrationservice.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import th.co.krungsri.service.registration.registrationservice.exception.apiexception.ApplicationException;
import th.co.krungsri.service.registration.registrationservice.model.BaseModelResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Map;

@ControllerAdvice
@Component
@Slf4j
public class ApplicationExceptionHandler {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Object handleAnyException(HttpServletRequest request, Exception ex) {
        log.info("==== handleException() ====");
        BaseModelResponse response;
        if (ApplicationRuntimeException.class.isInstance(ex)) {
            ApplicationException exception = (ApplicationException) ex;
            response = exception.getBaseModelResponse();
            return response;
        }

        response = new BaseModelResponse();
        response.setResponseCode("Regis-000");
        response.setResponseMessage("พบข้อผิดพลาดจากระบบ");
        response.setTechnicalMessage(ex.toString());
        response.setNamespace(String.valueOf(request.getServletPath()));
        return response;
    }

}
