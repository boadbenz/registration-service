package th.co.krungsri.service.registration.registrationservice.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class GenerateReferenceCode {

    public static String generateRefCode(String phone){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYYMMdd");
        return LocalDate.now().format(formatter) + "" + phone.substring(phone.length() - 4);
    }
}
