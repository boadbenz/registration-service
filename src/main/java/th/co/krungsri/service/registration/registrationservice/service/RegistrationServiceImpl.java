package th.co.krungsri.service.registration.registrationservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import th.co.krungsri.service.registration.registrationservice.entity.Member;
import th.co.krungsri.service.registration.registrationservice.exception.apiexception.ApplicationException;
import th.co.krungsri.service.registration.registrationservice.model.RegisterModel;
import th.co.krungsri.service.registration.registrationservice.model.RegisterResponse;
import th.co.krungsri.service.registration.registrationservice.model.UserInfoResponse;
import th.co.krungsri.service.registration.registrationservice.repository.MemberRepository;
import th.co.krungsri.service.registration.registrationservice.repository.MemberTypeRepository;
import th.co.krungsri.service.registration.registrationservice.utils.GenerateReferenceCode;

@Slf4j
@Service
public class RegistrationServiceImpl implements RegistrationService {

    enum Type{
        Platinum , Gold, Silver
    }

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private MemberTypeRepository memberTypeRepository;

    private MemberRepository memberRepository;

    @Autowired
    public RegistrationServiceImpl(BCryptPasswordEncoder bCryptPasswordEncoder, MemberTypeRepository memberTypeRepository,
                                   MemberRepository memberRepository) {

        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.memberRepository = memberRepository;
        this.memberTypeRepository = memberTypeRepository;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public RegisterResponse register(RegisterModel member) throws ApplicationException {
        log.info("================ RegistrationService register--> {} ================ ", member);
        RegisterResponse registerResponse = new RegisterResponse();
        Member entityMember = new Member();
        String mobilePhone = member.getPhone().replaceAll("-", "");
        entityMember.setPhone(mobilePhone);
        entityMember.setUsername(member.getUsername());
        entityMember.setSalary(member.getSalary());
        entityMember.setAddress(member.getAddress());
        entityMember.setPassword(bCryptPasswordEncoder.encode(member.getPassword()));
        String refCode = GenerateReferenceCode.generateRefCode(mobilePhone);
        entityMember.setReferenceCode(Long.valueOf(refCode));
        if(member.getSalary() >= 50000)
            entityMember.setMemberType(memberTypeRepository.findByName(Type.Platinum.toString()));
        else if(member.getSalary() >= 30000 && member.getSalary() < 50000)
            entityMember.setMemberType(memberTypeRepository.findByName(Type.Gold.toString()));
        else if(member.getSalary() >= 15000 && member.getSalary() < 30000)
            entityMember.setMemberType(memberTypeRepository.findByName(Type.Silver.toString()));
        else
            throw new ApplicationException("Regis-001","Cannot Register Salary Less Than 15,000", "salary < 15,000");
        log.info("================ RegistrationService MemberType --> {} ================ ", entityMember.getMemberType().getName());
        registerResponse.setMemberType(entityMember.getMemberType().getName());
        log.info("================ RegistrationService save --> {} ================ ", entityMember);
        memberRepository.save(entityMember);

        return registerResponse;
    }

    @Override
    public UserInfoResponse getUserInfo(String userName) {
        log.info("================ RegistrationService getUserInfo --> {} ================ ", userName);
        UserInfoResponse userInfoResponse = new UserInfoResponse();
        Member member = memberRepository.findByUsername(userName);
        userInfoResponse.setAddress(member.getAddress());
        userInfoResponse.setPhone(member.getPhone());
        userInfoResponse.setSalary(member.getSalary());
        userInfoResponse.setUsername(member.getUsername());
        userInfoResponse.setReferenceCode(member.getReferenceCode());
        userInfoResponse.setPassword(member.getPassword());
        userInfoResponse.setMemberType(member.getMemberType().getName());
        log.info("================ RegistrationService response --> {} ================ ", userInfoResponse);
        return userInfoResponse;
    }
}
