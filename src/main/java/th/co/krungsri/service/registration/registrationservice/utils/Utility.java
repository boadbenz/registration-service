package th.co.krungsri.service.registration.registrationservice.utils;

public class Utility {

	public static String upperFirstChar(String string) {
		return string.substring(0,1).toUpperCase() + string.substring(1);
	}

	public static String doSetMethod(String string) {
		return "set"+upperFirstChar(string);
	}

	public static String doGetMethod(String string) {
		return "get"+upperFirstChar(string);
	}
}
