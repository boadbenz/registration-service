package th.co.krungsri.service.registration.registrationservice.repository;

import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import th.co.krungsri.service.registration.registrationservice.entity.MemberType;

@RepositoryRestResource(collectionResourceRel = "member-type",path = "member-type")
public interface MemberTypeRepository extends BaseRepository<MemberType, Long> {

    MemberType findByName(@Param("name") String name);
}
