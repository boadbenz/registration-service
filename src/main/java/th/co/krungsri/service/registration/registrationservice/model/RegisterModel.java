package th.co.krungsri.service.registration.registrationservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RegisterModel {

    @JsonProperty(value = "username")
    private String username;

    @JsonProperty(value = "password")
    private String password;

    @JsonProperty(value = "address")
    private String address;

    @JsonProperty(value = "phone")
    private String phone;

    @JsonProperty(value = "salary")
    private Long salary;
}
