package th.co.krungsri.service.registration.registrationservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import th.co.krungsri.service.registration.registrationservice.model.RegisterModel;
import th.co.krungsri.service.registration.registrationservice.model.RegisterResponse;
import th.co.krungsri.service.registration.registrationservice.model.UserInfoResponse;
import th.co.krungsri.service.registration.registrationservice.service.RegistrationService;

@Slf4j
@RestController
@RequestMapping("/api/v1/registration")
public class RegistrationController {

    @Autowired
    RegistrationService registrationService;

    @PostMapping(value="/register")
    public RegisterResponse register(@RequestBody RegisterModel member) throws Exception{
        return registrationService.register(member);
    }


    @GetMapping(value = "/get-info")
    public UserInfoResponse getUserInfo(@RequestParam(value = "username")String userName) throws Exception{
        return registrationService.getUserInfo(userName);
    }

}
