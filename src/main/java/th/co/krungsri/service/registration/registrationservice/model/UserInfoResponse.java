package th.co.krungsri.service.registration.registrationservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UserInfoResponse extends BaseModelResponse{

    @JsonProperty(value = "username")
    private String username;

    @JsonProperty(value = "password")
    private String password;

    @JsonProperty(value = "address")
    private String address;

    @JsonProperty(value = "phone")
    private String phone;

    @JsonProperty(value = "salary")
    private Long salary;

    @JsonProperty(value = "reference_code")
    private Long referenceCode;

    @JsonProperty(value = "member_type")
    private String memberType;
}
