package th.co.krungsri.service.registration.registrationservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RegisterResponse extends BaseModelResponse{

    @JsonProperty(value = "member_type")
    private String memberType;
}
