package th.co.krungsri.service.registration.registrationservice.exception.apiexception;

import th.co.krungsri.service.registration.registrationservice.exception.ApplicationRuntimeException;
import th.co.krungsri.service.registration.registrationservice.model.BaseModelResponse;

public class ApplicationException extends ApplicationRuntimeException {

    /**
     *
     * @param code : Response Code.
     * @param responseMessage : Message Response.
     * @param technicalMessage : Default error message for developer.
     */
    public ApplicationException(String code, String responseMessage, String technicalMessage) {
        super(code, responseMessage, technicalMessage);
    }
}
